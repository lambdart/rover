# Define elixir invocation
ELIXIR = elixir

# Define mix invocation
MIX = mix

# Define mix compile argument
COMPILE = compile

# Define mix test argument
TEST = test

# Define elixir script file that will be run by it
SCRIPT = lib/rover.ex

# Define the input
INPUT = misc/input/input.txt

# Default goal
all: compile

# mix compile
compile:
	${MIX} ${COMPILE} < ${INPUT}

clean:
	@(${MIX} ${clean})

run:
	(${ELIXIR} ${SCRIPT})
