#!/bin/sh

# auto helper script

# possible actions
_ACTIONS="help,run,run_test,run_docker,clean"

# colors
_RED=""
_GREEN=""
_RESET=""
_YELLOW=""

# application root directory
_ROOT_DIR=.

# action: default build the project
_ACTION="run_docker"

# flags
_YES_FLAG=1

usage ()
{
    # print help message
    printf $_GREEN
    cat >&1 <<EOF

    [ AUTO HELPER SCRIPT ]

    usage: ${0} [-yxve] [-R {root_dir}] {ACTION}

    ACTIONS:

    help                  show this message and exit
    opts                  show default options
    run                   execute the rover script
    clean                 run mix clean (just a make wrapper)
    run_test              run a quick test using an predefined input (misc/input/input.txt)
    run_docker            construct the docker image and run rover script

    OPTIONS:

    -x                    enables debug capability
    -v                    enables verbose capability
    -y                    answer 'YES' for all questions
    -e                    exit immediately if any untested command fails in
                          non-interactive mode (not recommended)

    -R {root_dir}         set application source directory, default: .

EOF
    printf $_RESET
}

set_colors ()
{
    # True if the file whose file descriptor number is
    # file_descriptor is open and is associated with a terminal.
    # test(1)
    test -t 1 && {
        _RED="\033[31m"
        _GREEN="\033[32m"
        _YELLOW="\033[33m"
        _RESET="\033[m"
    }
}

# logs, printf wrapper
nlog () { printf "$@"; }

# error
log_error () { printf $_RED; log "$@"; printf $_RESET; }

# success
log_ok () { printf $_GREEN; log "$@"; printf $_RESET; }

# fatal, die: logs error and exit
die () { log_error "[-] Fatal: $@"; exit 1; }

# help message related to options
show_opts ()
{
    log "
[*] Options Values:

ACTION = ${_ACTION}
\n"
}

# help message related to actions
show_actions ()
{
    # local field separator
    local IFS=","

    # help message
    log_ok "available actions: "

    # show available actions
    for _ in ${_ACTIONS}; do
        log "${_} "
    done

    # keep good and clean (layout)
    log "\n"
}

check_action ()
{
    # local field separator
    local IFS=","

    # find action
    for _ in ${_ACTIONS}; do
        # found
        test "${_}" = "${_ACTION}" && return 0
    done

    # logs and leave
    log_error "action not found\n"

    # help message and leave
    show_actions && exit 1
}

# parse user options
parse_opts ()
{
    # get options
    for opt in "$@"; do
        case $opt in
            # enable debugging
            -x) shift 1; set -x ;;

            # enable verbose
            -v) shift 1; set -v ;;

            # enable errexit
            -e) shift 1; set -e ;;

            # set YES to the answers
            -y) shift 1; _YES_FLAG=0 ;;

            # set directories
            -R) shift 1; _ROOT_DIR=${1}; shift 1 ;;
        esac
    done

    # set action
    _ACTION=${1}

    # verify action argument
    check_action
}

# show values and operations that will be executed to the user
# before execute them
confirm_opts ()
{
    # if yes flag equal true (0), don't ask just leave
    test ${_YES_FLAG} -eq 0 && \
        return 0

    # show question
    log "[*] [h] help\n"
    log "[*] Can we proceed? [y/n/h] : "

    while read answer
    do
        case ${answer} in
            # possible answers: (yes, no, help)
            y|yes) _YES_FLAG=0 ;;

            # just leave
            n|no) log "\n"; exit 0 ;;

            # show help and leave
            h|help) usage; exit 0 ;;

            # undefined: asks again
            *) log "[*] Can we proceed? [y/n/h] : " ;;
        esac
        # test the yes flag, if true leave the while loop
        test ${_YES_FLAG} -eq 0 && break;
    done
    return 0
}

clean_rover ()
{
    # make install
    make -C ${_ROOT_DIR} clean
}

# execute rover passing < input to it
run_rover ()
{
    make -C ${_ROOT_DIR} run
}

# execute rover passing input.txt to it
run_rover_test ()
{
    make -C ${_ROOT_DIR} run <  ${_ROOT_DIR}/misc/input/input.txt
}

# handle information actions
# some opcodes/functions aren't 'dangerous',
# handle it directly here
handle_info ()
{
    # switch/case/try/catch equivalent
    case $_ACTION in
        # show usage (help) and force leave
        help) usage; exit 0 ;;

        # show options
        opts) show_opts ;;

        # not handled here, continue
        *) return 0 ;;
    esac

    # handled
    exit 0
}

docker_run ()
{
    docker run -it rover:elixir
}

docker_build ()
{
    docker build -t rover:elixir ${_ROOT_DIR}
}

run_docker ()
{
    docker_build && docker_run
}

# select an action and execute it associated function
handle_action ()
{
    # switch/case/try/catch equivalent
    case $_ACTION in
        # run the rover script
        run) run_rover ;;

        # run a quick test using a predefined input
        run_test) run_rover_test ;;

        # build docker image/container compile and execute rover.scm (the  )
        run_docker) run_docker ;;

        # clean rover
        clean) clean_rover ;;

        # action not found (probably will never reach this portion of the code)
        # just leave it here to handle unexpected errors
        *) die "invalid action, $0 help for more information" ;;
    esac
}

# main entry point
main ()
{
    # get/set user options (command-line)
    parse_opts "$@"

    # handle information actions (opcodes)
    # help, show_opts, etc..
    handle_info

    # show options
    show_opts

    # asks for confirmation
    confirm_opts

    # select build related actions and handle it
    handle_action
}

# setup colors variables
set_colors

# main routine
main "$@"
