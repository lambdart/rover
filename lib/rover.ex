# import (just reference)

# not used just a reference for the future session (README.org)
defmodule Probe do
  defstruct x: 0, y: 0, d: 'N'
end

# Test solution module
defmodule Rover do

  @moduledoc """
  My solution for the mars explorer (land probe) problem.

  """

  # select the right windrose character representation.
  defp windrose(i) do
    %{0 => 'N',
      1 => 'W',
      2 => 'S',
      3 => 'E'}[i]
  end

  @doc """
  Rotate 90 degrees clockwise, following the four
  cardinals directions, represented here by the map:

  Directions : {0 => 'N', 1 => 'W', 2 => 'S', 3 => 'E'}

  """
  def rotate_left(i) do
    # equivalent: %{0 => 1, 1 => 2, 2 => 3, 3 => 0}[i]
    rem((i + 1), 4)
  end

  @doc """
  Rotate 90 degrees anticlockwise, following the four (N, S, W, E)
  cardinals directions, represented here by the map:

  Directions : {0 => 'N', 1 => 'W', 2 => 'S', 3 => 'E'}

  """
  def rotate_right(i) do
    # equivalent: %{0 => 3, 1 => 0, 2 => 1, 3 => 2}[i]
    f = fn
      x when x == 0 -> 3
      x -> x - 1
    end
    f.(i)
  end

  @doc """
  Calculate the plus factor (0, 1 or -1), this
  depends on the direction and watch point we want
  to move:

  x : move(west, east)   : move(left, right) : x - 1, x + 1
  y : move(north, south) : move(up, down)    : y - 1, y + 1

  """
  def factor(point, direction) do
    # get the right key (N, S..)
    key = windrose(direction)
    # select the right factor
    case point do
      'x' -> %{'S' => 0,  'N' => 0, 'W' => -1, 'E' => 1}[key]
      'y' -> %{'S' => -1, 'N' => 1, 'W' => 0,  'E' => 0}[key]
      _  -> 0
    end
  end

  @doc """
  Select and calculate the final value of the
  movement.

  """
  # another way, maybe more legible (another option)
  def move(command, point, value, direction) do
    if command == "M" do
      value + factor(point, direction)
    else
      value
    end
  end

  @doc """
  Selects and computes the rotation: left or right.

  Just a alternative function that does the same move computation
  but with another syntax (maybe more clear to the reader).

  """
  def rotate_alt(command, direction) do
    case command do
      "L" -> rotate_left(direction)
      "R" -> rotate_right(direction)
      _  -> direction
    end
  end

  @doc """
  Verify if the command (left or right) is related to the rotate
  operation and computes it.

  """
  def rotate(command, direction) do
    f = fn
      c, d when c == "L" -> rotate_left(d)
      c, d when c == "R" -> rotate_right(d)
      _c, d -> d
    end
    f.(command, direction)
  end

  @doc """

  Land the space-probe which means: compute all the commands
  in a recursive loop and calculate the final position on the
  grid.

  Examples:

  iex> Rover.land_probe("MMRMMRMRRM" |> String.graphemes, 3, 3, 3)
  {5, 1, 'E'}

  """
  def land_probe([first | rest], direction, x, y) do
    # update direction
    direction = rotate(first, direction)

    # lambda function to update point x or y position
    f = fn
      p, c, v when c == "M" -> p + v
      p, _, _ -> p
    end

    # lambda functions to get the plus factor
    df = fn d, t -> t |> elem(d) end

    # update (maybe) x and y points
    x = f.(x, first, df.(direction, {0, -1, 0, 1}))
    y = f.(y, first, df.(direction, {1, 0, -1, 0}))

    # recursive call: for loop equivalent
    land_probe(rest, direction, x, y)
  end

  # Last matching of land-probe function, just 'return' the probes
  # attributes (the solution) {x, y, (N|S|W|E)}:
  # the probe final position on the grid.
  def land_probe([], direction, x, y) do
    [x, y, windrose(direction)] |> Enum.join(" ")
  end

  defp solution([first | rest], grid) do
    # parse position and commands
    position = first |> String.split |> List.to_tuple
    commands = rest  |> hd |> String.graphemes

    # parse x and y
    x = position |> elem(0) |> String.to_integer
    y = position |> elem(1) |> String.to_integer

    # parse direction
    direction = position
    |> elem(2)
    |> fn k -> %{"N" => 0,
                "W" => 1,
                "S" => 2,
                "E" => 3}[k] end.()

    # land the probe and output its
    land_probe(commands, direction, x, y) |> IO.puts

    # loop over
    solution(tl(rest), grid)
  end

  defp solution([], _grid) do
    :ok
  end

  defp get_lines({:error, reason}, _list), do: {:error, reason}

  defp get_lines(:eof, list), do: list

  defp get_lines(data, list) do
    # get line
    line = data |> String.trim |> String.split("\n")
    # append to the list
    get_lines(IO.gets(""), list ++ line)
  end

  @doc """
  Rover module entry point: main function.

  """
  def main do
    # get lines
    [grid | rest] = get_lines(IO.gets("Please, enter the Rover controller input: \n"), [])

    # run land probe solution
    solution(rest, grid)
  end
end

Rover.main()
