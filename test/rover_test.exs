defmodule RoverTest do
  use ExUnit.Case
  doctest Rover

  test "Testing:land_probe" do
    assert Rover.land_probe(["M", "M", "R", "M", "M", "R", "M", "R", "R", "M"], 3, 3, 3) == "5 1 E"
  end
end
