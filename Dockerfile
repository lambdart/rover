FROM alpine:edge

# Install gcc and chicken package
RUN apk update && apk add --no-cache elixir make

# remove cache
RUN rm -rf /tmp/* /var/cache/apk/*

# Create app directory
RUN mkdir /app

# Copy all files to the app directory (image space)
# TODO: Maybe a volume here would be a better options
COPY . /app

# Container entry point: run the bof game (using the helper auto-script, more generic)
ENTRYPOINT ["/app/misc/sh/auto.sh", "-y", "-R", "/app", "run"]

# Alternative form (simple and static)
# ENTRYPOINT ["elixir", "/app/lib/rover.ex"]
